import './index.scss'

const Documentation = () => {
  return (
    <div>
      <h1>react-element-admin</h1>
      <h3>1、尝试使用react</h3>
      <h3>2、尝试使用element-react</h3>
      <h3>3、对vue-element-admin的模仿</h3>
      <a className="documentation__link"
        href="https://panjiachen.github.io/vue-element-admin-site/zh/guide/"
        rel="noreferrer"
        target="_blank"
      >
        vue-element-admin项目
      </a>
    </div>
  )
}

export default Documentation
