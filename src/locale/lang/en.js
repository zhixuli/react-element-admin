const en = {
  login: {
    title: 'Login Form',
    login: 'Login',
    username: 'Username',
    password: 'Password'
  }
}

export default en
